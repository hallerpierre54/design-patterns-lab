package pattern.adapter;

import java.util.Observable;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class AdapterLegacyTemperature extends Observable implements ISensor  {

	private LegacyTemperatureSensor lts;
	
	public AdapterLegacyTemperature(LegacyTemperatureSensor lts) {
		this.lts = lts;
	}
	
	
	@Override
	public void on() {
		if(!this.lts.getStatus())
			this.lts.onOff();
	}

	@Override
	public void off() {
		if(this.lts.getStatus())
			this.lts.onOff();
	}

	@Override
	public boolean getStatus() {
		return this.lts.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if(!this.lts.getStatus()){
			throw new SensorNotActivatedException("Capteur �teind");
		}
		// on signale a la vue que la valeur a chang�
		setChanged();
        notifyObservers(this);
		return;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if(!this.getStatus())
			throw new SensorNotActivatedException("Capteur �teind");
		
		return this.lts.getTemperature();
	}

}
