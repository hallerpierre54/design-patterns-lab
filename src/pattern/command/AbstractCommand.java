package pattern.command;

import eu.telecomnancy.sensor.ISensor;

public abstract class AbstractCommand implements TemperatureSensorCommand{
	
	ISensor sensor;
	
	public AbstractCommand(ISensor is){
		this.sensor = is;
	}
}
