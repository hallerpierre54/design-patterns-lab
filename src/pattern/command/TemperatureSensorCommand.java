package pattern.command;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface TemperatureSensorCommand {
	public void execute() throws SensorNotActivatedException;
}
