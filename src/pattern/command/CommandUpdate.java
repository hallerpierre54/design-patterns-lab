package pattern.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandUpdate extends AbstractCommand{

	public CommandUpdate(ISensor is) {
		super(is);
	}

	@Override
	public void execute() throws SensorNotActivatedException {
		this.sensor.update();
	}

}
