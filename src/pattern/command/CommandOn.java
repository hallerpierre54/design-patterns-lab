package pattern.command;

import eu.telecomnancy.sensor.ISensor;

public class CommandOn extends AbstractCommand{

	public CommandOn(ISensor is) {
		super(is);
	}

	@Override
	public void execute() {
		this.sensor.on();
		
	}

}
