package pattern.command;

import eu.telecomnancy.sensor.ISensor;

public class CommandOff extends AbstractCommand{

	public CommandOff(ISensor is) {
		super(is);
	}

	@Override
	public void execute() {
		this.sensor.off();
	}

}
