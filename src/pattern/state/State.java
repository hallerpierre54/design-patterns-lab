package pattern.state;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public abstract class State {
	
	double value;

	public abstract double getValue() throws SensorNotActivatedException;
	
	public abstract boolean getStatus();
	
	public abstract void setValue(double v) throws SensorNotActivatedException;
}
