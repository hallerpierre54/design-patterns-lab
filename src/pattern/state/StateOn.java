package pattern.state;

public class StateOn extends State{

	@Override
	public boolean getStatus() {
		return true;
	}

	@Override
	public double getValue() {
		return this.value;
	}
	
	@Override
	public void setValue(double v){
		this.value = v;
	}
	
}
