package pattern.state;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class StateOff extends State{

	@Override
	public double getValue() throws SensorNotActivatedException{
		throw new SensorNotActivatedException("Sensor not active");
	}

	@Override
	public boolean getStatus() {
		return false;
	}

	@Override
	public void setValue(double v) throws SensorNotActivatedException{
		throw new SensorNotActivatedException("Sensor not active");

	}
	

}
