package pattern.decorator;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class TemperatureSensorCelsius extends TemperatureSensorDecorator{
	
	public double getValue() throws SensorNotActivatedException {
		return TemperatureSensorDecorator.round(super.getValue(), this.nbChiffres);
	}
	
	@Override
	public String getUnite(){
		return "�C";
	}
}
