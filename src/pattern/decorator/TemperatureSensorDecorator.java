package pattern.decorator;

import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public abstract class TemperatureSensorDecorator extends TemperatureSensor {
	
	protected int nbChiffres = -1;
	
	public abstract String getUnite();

	public double getValue() throws SensorNotActivatedException{
		return super.getValue();
	}
	
	public void setChiffres(int nb){
		this.nbChiffres = nb;
	}

	/**
	 * permet d'arrondir un double
	 * 
	 * @param value
	 *            : la valeur � arrondir
	 * @param places
	 *            : le nombre de chiffres apr�s la virgule
	 * @return
	 */
	public static double round(double value, int places) {
		if (places < 0)
			return value;

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}
	
	public void setValue(double v){
		value = v;
	}

}
