package pattern.decorator;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class TemperatureSensorFarheneit extends TemperatureSensorDecorator {
	
	@Override
	public double getValue() throws SensorNotActivatedException {
		return TemperatureSensorDecorator.round(1.8 * super.getValue() + 32, this.nbChiffres);
	}

	@Override
	public String getUnite() {
		return "�F";
	}
}
