package pattern.proxy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LogLevel;
import eu.telecomnancy.sensor.SensorLogger;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class TemperatureSensorProxy extends TemperatureSensor implements ISensor,
		SensorLogger {
	
	public TemperatureSensorProxy() {
		super();
	}

	@Override
	public void on() {
		super.on();
		this.log(LogLevel.INFO, "Fonction on() appel� : Capteur on");
	}

	@Override
	public void off() {
		super.off();
		this.log(LogLevel.INFO, "Fonction off() appel� : Capteur off");
	}

	@Override
	public boolean getStatus() {
		boolean state = super.getStatus();
		this.log(LogLevel.INFO, "Fonction getStatus() appel� : " + state);
		return state;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		try {
			super.update();
			this.log(LogLevel.INFO, "Fonction update() appel� : update");
		} catch (SensorNotActivatedException e) {
			this.log(LogLevel.FATAL,
					"Fonction update() appel� : ERREUR CAPTEUR ETEIND");
			throw new SensorNotActivatedException(
					"Sensor must be activated before acquiring new values.");
		}
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		try {
			double value = super.getValue();
			this.log(LogLevel.INFO, "Fonction getValue() appel� : " + value);
			return value;
		} catch (SensorNotActivatedException e) {
			this.log(LogLevel.FATAL,
					"Fonction getValue() appel� : ERREUR CAPTEUR ETEIND");
			throw new SensorNotActivatedException(
					"Sensor must be activated before acquiring new values.");
		}
	}

	@Override
	public void log(LogLevel level, String message) {
		
		switch (level) {
		case INFO:
			System.out.println("------\tLOG\t------");
			System.out.println(getDate() + " : \n" + message);
			System.out.println("------\tEND LOG\t------\n");
			break;
		case FATAL:
			System.err.println("------\tLOG\t------");
			System.err.println(getDate() + " : \n" + message);
			System.err.println("------\tEND LOG\t------\n");
			break;
		default:
			System.out.println("Loglevel non impl�ment�");
			break;
		}
		
	}

	public static String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime()); // 07/12/2015 16:00:22
	}

}
