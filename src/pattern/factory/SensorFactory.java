package pattern.factory;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;

public class SensorFactory extends AbstractSensorFactory{

	@Override
	public ISensor getSensor() {
		return new TemperatureSensor();
	}

}
