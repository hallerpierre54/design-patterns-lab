package pattern.factory;

import pattern.adapter.AdapterLegacyTemperature;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;

public class AdapterSensorFactory extends AbstractSensorFactory{

	@Override
	public ISensor getSensor() {
		return new AdapterLegacyTemperature(new LegacyTemperatureSensor());
	}
	
}
