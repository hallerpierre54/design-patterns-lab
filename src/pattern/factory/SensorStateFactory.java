package pattern.factory;

import pattern.state.TemperatureSensorState;
import eu.telecomnancy.sensor.ISensor;

public class SensorStateFactory extends AbstractSensorFactory{

	@Override
	public ISensor getSensor() {
		return new TemperatureSensorState();
	}

}
