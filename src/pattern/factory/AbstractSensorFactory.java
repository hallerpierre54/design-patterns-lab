package pattern.factory;

import java.io.IOException;
import java.util.Properties;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.ISensor;

public abstract class AbstractSensorFactory {
	
    public static ISensor makeSensor() {
        ReadPropertyFile rp = new ReadPropertyFile();
        Properties p = null;
        try {
            p = rp.readFile("/eu/telecomnancy/app.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String factory = p.getProperty("factory");
        ISensor sensor;
        sensor = null;
        try {
            AbstractSensorFactory sensorfactory = (AbstractSensorFactory) Class.forName(factory).newInstance();
            sensor = sensorfactory.getSensor();

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return sensor;
    }

    public abstract ISensor getSensor();
}