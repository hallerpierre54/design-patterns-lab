package pattern.factory;

import pattern.proxy.TemperatureSensorProxy;
import eu.telecomnancy.sensor.ISensor;

public class SensorProxyFactory extends AbstractSensorFactory{

	@Override
	public ISensor getSensor() {
		return new TemperatureSensorProxy();
	}

}
