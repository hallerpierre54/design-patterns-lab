package eu.telecomnancy;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import pattern.decorator.TemperatureSensorCelsius;
import pattern.decorator.TemperatureSensorDecorator;
import pattern.decorator.TemperatureSensorFarheneit;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.ui.ConsoleUI;

public class AppTestDecorator {

	private TemperatureSensorDecorator sensor;
	private Scanner console;

	public static void main(String[] args) {
		TemperatureSensorDecorator sensor = new TemperatureSensorCelsius();
		AppTestDecorator test = new AppTestDecorator();
		test.consoleUI(sensor);
	}

	public void consoleUI(TemperatureSensorDecorator sensor) {
		this.sensor = sensor;
		this.console = new Scanner(System.in);
		manageCLI();
	}

	public void manageCLI() {
		String rep = "";
		System.out
				.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - change unit|c: changUnit - value|v: value"
						+ "changePrecis|`cp: Change Pr�cision");
		while (!"q".equals(rep)) {
			try {
				System.out.print(":> ");
				rep = this.console.nextLine();
				if ("on".equals(rep) || "o".equals(rep)) {
					this.sensor.on();
					System.out.println("sensor turned on.");
				} else if ("off".equals(rep) || "O".equals(rep)) {
					this.sensor.off();
					System.out.println("sensor turned off.");
				} else if ("status".equals(rep) || "s".equals(rep)) {
					System.out.println("status: " + this.sensor.getStatus());
				} else if ("update".equals(rep) || "u".equals(rep)) {
					this.sensor.update();
					System.out.println("sensor value refreshed.");
				} else if ("value".equals(rep) || "v".equals(rep)) {
					System.out.println("value: " + this.sensor.getValue()
							+ this.sensor.getUnite());
				}
				// TEST DE LA FONCTION DE CHANGEMENT D'UNITE
				else if ("changeUnit".equals(rep) || "c".equals(rep)) {
					if (sensor instanceof TemperatureSensorCelsius) {
						TemperatureSensorFarheneit sensortmp = new TemperatureSensorFarheneit();
						if (this.sensor.getStatus()) {
							sensortmp.setValue(this.sensor.getValue());
							sensortmp.on();
						}
						this.sensor = sensortmp;
						System.out.println("Unite changed to Farheneit");
					} else {
						TemperatureSensorCelsius sensortmp = new TemperatureSensorCelsius();
						if (this.sensor.getStatus()) {
							sensortmp.setValue((this.sensor.getValue() - 32)/1.8);
							sensortmp.on();
						}
						this.sensor = sensortmp;
						System.out.println("Unite changed to Celsius");
					}
				} 
				// TEST DE LA FONCTION DE CHANGEMENT DE PRECISION
				else if ("changePrecis".equals(rep) || "cp".equals(rep)){
					System.out.println("Nombre de chiffres apr�s la virgule : ");
					int nb = this.console.nextInt();
					this.sensor.setChiffres(nb);
				} else {
					System.out
							.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value");
				}
			} catch (SensorNotActivatedException ex) {
				Logger.getLogger(ConsoleUI.class.getName()).log(Level.SEVERE,
						null, ex);
			}
		}
	}
}
