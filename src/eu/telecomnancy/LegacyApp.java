package eu.telecomnancy;

import pattern.adapter.AdapterLegacyTemperature;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class LegacyApp {
	
	public static void main(String[] args) {
		LegacyTemperatureSensor lts = new LegacyTemperatureSensor();
		ISensor sensor = new AdapterLegacyTemperature(lts);
	    new ConsoleUI(sensor);
	}
	
}
