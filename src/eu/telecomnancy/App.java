package eu.telecomnancy;

import java.util.Scanner;

import pattern.adapter.AdapterLegacyTemperature;
import pattern.factory.AbstractSensorFactory;
import pattern.proxy.TemperatureSensorProxy;
import pattern.state.TemperatureSensorState;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
    	/*
    	 *	Pour tester les patterns
    	 */
    	Scanner sc = new Scanner(System.in);
		System.out
				.println("1 : TemperatureSensor\n"
						+ "2 : AdapterLegacyTemperature\n"
						+ "3 : TemperatureSensorState\n"
						+ "4 : TemperatureSensorProxy\n"
						+ "5 : Factory");
		ISensor sensor = null;
		int choix = sc.nextInt();
				
		switch (choix) {
		case 1:
			sensor = new TemperatureSensor();
			break;
		case 2:
			sensor = new AdapterLegacyTemperature(new LegacyTemperatureSensor());
			break;
		case 3:
			sensor = new TemperatureSensorState();
			break;
		case 4:
			sensor = new TemperatureSensorProxy();
			break;
		case 5:
			sensor = AbstractSensorFactory.makeSensor();
			break;
		default:
			System.out.println("Choix non compris");
			sc.close();
			return;
		}
		
        new ConsoleUI(sensor);
        sc.close();
    }

}