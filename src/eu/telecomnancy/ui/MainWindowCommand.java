package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;

import javax.swing.*;

import java.awt.*;
import java.util.Observable;

public class MainWindowCommand extends JFrame {

	private ISensor sensor;
	private SensorViewCommand sensorViewCommand;

	public MainWindowCommand(ISensor sensor) {
		this.sensor = sensor;
		this.sensorViewCommand = new SensorViewCommand(this.sensor);

		((Observable) this.sensor).addObserver(this.sensorViewCommand);

		this.setLayout(new BorderLayout());
		this.add(this.sensorViewCommand, BorderLayout.CENTER);

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

}
