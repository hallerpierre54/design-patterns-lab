package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;

import javax.swing.*;

import java.awt.*;
import java.util.Observable;

public class MainWindow extends JFrame {

	private ISensor sensor;
	private SensorView sensorView;

	public MainWindow(ISensor sensor) {
		this.sensor = sensor;
		this.sensorView = new SensorView(this.sensor);

		((Observable) this.sensor).addObserver(this.sensorView);

		this.setLayout(new BorderLayout());
		this.add(this.sensorView, BorderLayout.CENTER);

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

}
