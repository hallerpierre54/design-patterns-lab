package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import pattern.command.TemperatureSensorCommand;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;


public class SensorViewCommand extends JPanel implements Observer {
	private ISensor sensor;

	private JLabel value = new JLabel("N/A �C");


	public SensorViewCommand(ISensor c) {
		this.sensor = c;
		this.setLayout(new BorderLayout());

		value.setHorizontalAlignment(SwingConstants.CENTER);
		Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
		value.setFont(sensorValueFont);

		this.add(value, BorderLayout.CENTER);
		
		
		//GESTION COMMANDES
		HashMap<String, String> commandes = null;
		try {
			commandes = readProperties();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Collection<String> commandesStrings = commandes.values();
		String[] array = commandesStrings.toArray(new String[commandesStrings.size()]);
		
		JComboBox<String> commandesList = new JComboBox<String>(array);
		commandesList.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox)e.getSource();
		        String commandeName = (String)cb.getSelectedItem();
		        System.out.println(commandeName);
		        try {
			        Class<?> c = Class.forName(commandeName);
			        Constructor<?> cons = c.getConstructor(ISensor.class);
					Object object = cons.newInstance(sensor);
					((TemperatureSensorCommand)object).execute();
					

				} catch (InstantiationException | IllegalAccessException
						| IllegalArgumentException | InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NoSuchMethodException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});


		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new GridLayout(1, 3));
		buttonsPanel.add(commandesList);

		this.add(buttonsPanel, BorderLayout.SOUTH);
	}

	@Override
	public void update(Observable sens, Object arg1) {
		if (sens instanceof ISensor) {
			sensor = (ISensor) sens;
			try {
				value.setText(sensor.getValue() + "");
				repaint();
			} catch (SensorNotActivatedException e) {
				e.printStackTrace();
			}
		}

	}

	private HashMap<String, String> readProperties() throws IOException {
		HashMap<String, String> commandes = new HashMap<>();
		
		InputStream inputStream = null;
		try {
			Properties prop = new Properties();
			String propFileName = "eu/telecomnancy/commande.properties";

			inputStream = getClass().getClassLoader().getResourceAsStream(
					propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '"
						+ propFileName + "' not found in the classpath");
			}

			// get the property value and print it out
			String commandeOn = prop.getProperty("commandeOn");
			String commandeOff = prop.getProperty("commandeOff");
			String commandeUpdate = prop.getProperty("commandeUpdate");
			String commandeGetValue = prop.getProperty("commandeGetValue");

			commandes.put("commandeOn", commandeOn);
			commandes.put("commandeOff", commandeOff);
			commandes.put("commandeUpdate", commandeUpdate);
			commandes.put("commandeGetValue", commandeGetValue);
			
			
			
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return commandes;

	}
}
